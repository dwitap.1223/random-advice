import MainLayout from '@layouts/MainLayout';

import Home from '@pages/Home';
import NotFound from '@pages/NotFound';
import randAdvice from '@pages/Advice';

const routes = [
  {
    path: '/',
    name: 'Advice',
    component: randAdvice,
  },
  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
