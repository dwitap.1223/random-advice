import { connect, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { createStructuredSelector } from 'reselect';

import classes from './style.module.scss';
import PauseRoundedIcon from '@mui/icons-material/PauseRounded';
import CasinoIcon from '@mui/icons-material/Casino';

import { getAdvice } from '@containers/Advice/actions';
import { selectAdvice, selectLoading } from '@containers/Advice/selectors';
import Skeleton from '@mui/material/Skeleton';

const RandAdvice = ({ advice, loading, theme }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAdvice());
  }, []);

  const handleRefresh = () => {
    dispatch(getAdvice());
  };

  return (
    <div className={classes.mainContainer}>
      <div className={classes.contentContainer}>
        {advice && (
          <>
            {loading ? (
              <Skeleton variant="text" animation="wave" width={120} sx={{ mt: '4rem' }} />
            ) : (
              <>
                <div className={classes.number}>ADVICE #{advice.id}</div>
              </>
            )}
            {loading ? (
              <Skeleton variant="text" animation="wave" width={240} sx={{ mt: '3rem' }} />
            ) : (
              <>
                <div className={classes.quote}>"{advice.advice}"</div>
              </>
            )}
          </>
        )}
        <div className={classes.lineQuote}>
          <div className={classes.line}>
            <hr />
          </div>
          <PauseRoundedIcon />
          <div className={classes.line}>
            <hr />
          </div>
        </div>
      </div>
      <button>
        <CasinoIcon onClick={handleRefresh} sx={{ color: 'rgba(31, 38, 50, 1)' }} />
      </button>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  advice: selectAdvice,
  loading: selectLoading,
});

RandAdvice.propTypes = {
  advice: PropTypes.object,
  loading: PropTypes.bool,
};

export default connect(mapStateToProps)(RandAdvice);
