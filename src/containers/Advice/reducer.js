import { produce } from 'immer';

import { SET_ADVICE, SET_LOADING, GET_ADVICE } from './constants';

export const initialState = {
  advice: null,
  loading: false,
};

export const storedKeyQuote = ['advice'];

const adviceReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_ADVICE:
        draft.loading = true;
        break;
      case SET_ADVICE:
        draft.advice = action.advice;
        draft.loading = false;
        break;
      case SET_LOADING:
        draft.loading = action.loading;
        break;
      default:
        break;
    }
  });

export default adviceReducer;
