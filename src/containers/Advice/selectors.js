import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectAdviceState = (state) => state.advice || initialState;

export const selectAdvice = createSelector(selectAdviceState, (state) => state.advice);
export const selectLoading = createSelector(selectAdviceState, (state) => state.loading);
