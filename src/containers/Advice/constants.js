export const GET_ADVICE = 'advice/GET_ADVICE';
export const SET_ADVICE = 'advice/SET_ADVICE';
export const SET_LOADING = 'advice/SET_LOADING';
