import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_ADVICE } from './constants';
import { getAdviceLink } from '@domain/api';
import { setAdvice, setLoading } from './actions';

export function* doGetRandomAdvice() {
  try {
    const response = yield call(getAdviceLink);
    yield put(setAdvice(response.slip));
  } catch (err) {
    console.log(err);
  }
  yield put(setLoading(false));
}

export default function* adviceSaga() {
  yield takeLatest(GET_ADVICE, doGetRandomAdvice);
}
