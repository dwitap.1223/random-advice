import { GET_ADVICE, SET_ADVICE, SET_LOADING } from '@containers/Advice/constants';

export const getAdvice = () => ({
  type: GET_ADVICE,
});

export const setAdvice = (advice) => ({
  type: SET_ADVICE,
  advice,
});

export const setLoading = (loading) => ({
  type: SET_LOADING,
  loading,
});
